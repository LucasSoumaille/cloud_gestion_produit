# GESTION PRODUITS

## Issue 2
Pour utiliser l'appli : 

Installez docker, faire un : 

$ docker pull php:5.6-apache

$ docker pull mysql:5.7

$ docker pull minio/minio:latest

Une fois fait, se placer dans le dossier récupéré et faire : 

$ docker build -t cloud_tp_ls .

Une image se génèrera avec les informations et le contenu nécessaire.

Pour déployer l'application, se placer dans le dossier et faire : 

$ docker-compose up

L'application se lancera et il est possible de contacter l'appli web via localhost:8086 et le serveur minio via localhost:9001.

Pour arrêter l'application et les services faire : 

$ docker-compose down

## Prérequis
Cette application est compatible `PHP5` et a été testée avec une base de données `MySQL 5.7`.

## Installation
- Copier les fichiers du dossier `www` dans un dossier accessible par le serveur Web.
- Assurez vous que le dossier `uploads` est accessible en lecture et écriture par le serveur Web : `chmod 777 uploads`
- Importez la base de données test à partir du dump SQL `database/gestion_produits.sql`.
- Connectez vous à l'application avec l'url adaptée avec les informations suivantes :
    - Login : `admin`
    - Mot de passe : `password`

## Fonctionnalités
L'application permet de :
- Lister les produits
- Afficher la fiche produit en lecture seule
- Ajouter des produits
- Modifier les produits
- Supprimer les produits
- Pour chaque produit, il est possible d'ajouter autant de photos que nécessaire
