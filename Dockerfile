FROM php:5.6-apache

COPY ./www/ /var/www/html/

RUN docker-php-ext-install mysqli
RUN apt-get update -y
RUN apt-get install git -y
RUN apt-get install nano -y
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN php composer.phar require aws/aws-sdk-php

EXPOSE 80
